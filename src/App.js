import React from 'react';
import './App.css';
import WhoToFollowContainer from './components/WhoToContainer/WhoToFollowContainer';

function App() {
  return (
    <div className="App">
      <section>
        <WhoToFollowContainer />
      </section>
    </div>
  );
}

export default App;
