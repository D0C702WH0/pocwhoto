import React, { Component } from 'react'
import WhoToFollowCard from '../WhoToFollowCard/WhoToFollowCard'
import './WhoToFollowContainer.css'
import WhoToFollowCreate from '../WhoToFollowCreate/WhoToFollowCreate'

export class WhoToFollowContainer extends Component {
  constructor() {
    super()
    this.state = {
      whoTo: [
        {
          id: 1,
          firstName: "Jérémie",
          lastName: "Patonnier",
          description: " kdmqsdksqmldksqmldlksqd dmlqskdqsmldkdmlk lmdkqsmdlkqsdmlk",
          skill: "Vanilla js",
          twitter: "https://twitter.com/",
          github: "https://github.com/",
          facebook: "https://www.facebook.com/"
        },
        {
          id: 2,
          firstName: "Jonathan",
          lastName: "Barthélémy",
          description: " kdmqsdksqmldksqmldlksqd dmlqskdqsmldkdmlk lmdkqsmdlkqsdmlk",
          skill: "React",
          twitter: "https://twitter.com/",
          github: "https://github.com/",
          facebook: "https://www.facebook.com/"
        },
        {
          id: 3,
          firstName: "Norbert Jeff",
          lastName: "Nadir",
          description: " kdmqsdksqmldksqmldlksqd dmlqskdqsmldkdmlk lmdkqsmdlkqsdmlk",
          skill: "Public speaking",
          twitter: "https://twitter.com/",
          github: "https://github.com/",
          facebook: "https://www.facebook.com/"
        }
      ],
      isModalOpen: false
    }
    this.handleModalState = this.handleModalState.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

  }


  /**
   * Allows to control the modal open/close state
   */
  handleModalState() {
    const { isModalOpen } = this.state
    this.setState({ isModalOpen: !isModalOpen })
  }

  /**
   * Allows to get the datas from the create form
   * @param {*} data Object from the create form
   * @returns a new array of whoToFollow that includes the new datas
   */
  handleSubmit(data) {
    const { whoTo } = this.state;
    /** Close the modal */
    this.handleModalState()

    /** Add a new id to the object */
    data.id = this.state.whoTo.length + 1

    /** Create a new Array with the previous values and add the new object */
    const newArray = [...whoTo, data]
    this.setState({ whoTo: newArray })
  }



  render() {
    const { whoTo, isModalOpen } = this.state;
    const { handleModalState, handleSubmit } = this;

    /** Container of cards */
    const container =
      <div>
        <div className="navbar">
          <div className="buttonContainer">
            <button type="button" onClick={handleModalState} className="button is-primary is-pulled-right">Ajouter</button>
          </div>
        </div>
        <div className="row">
          {whoTo.length && whoTo.map(who => <WhoToFollowCard key={who.id} data={who} />)}
        </div>
      </div>

    return (
      <div className="container">
        {/** IF THE MODAL BOOLEAN IS FALSE WE DISPLAY THE CARDS */}
        {!isModalOpen && container}

        {/** ELSE WE DISPLAY THE CREATE FORM AND WE PASS THE PARENT METHODS AS PROPS */}
        {isModalOpen && <WhoToFollowCreate handleSubmit={handleSubmit} handleModalState={handleModalState} />}
      </div>
    )
  }
}

export default WhoToFollowContainer
