import React, { Component } from 'react'
import './WhoToFollowCard.css'

export class WhoToFollowCard extends Component {
  render() {
    const { data } = this.props
    return (
      <div className="cardContainer">
        <div className="card">
          <p className="title is-4">{data.firstName}</p>
          <p className="subtitle is-6">{data.lastName}</p>
          <div className="field">
            <span className="label">Spécialité</span>
            <p>{data.skill}</p>
          </div>
          <div className="content">
            <span className="label">Description</span>
            {data.description}
            <span className="label">Twitter</span>
            <a> {data.twitter}</a>
            <span className="label">Github</span>
            <a> {data.github}</a>
            <span className="label">FacebOok</span>
            <a> {data.facebook}</a>
          </div >
        </div>
      </div >
    )
  }
}

export default WhoToFollowCard
