import React, { Component } from 'react'
import './WhoToFollowCreate.css'

export class WhoToFollowCreate extends Component {

  constructor(props) {
    super(props)
    this.state = {
      firstName: "",
      lastName: "",
      description: "",
      skill: "",
      twitter: "",
      github: "",
      facebook: ""
    }
    this.handleChange = this.handleChange.bind(this)
    this.submitNewWhoTo = this.submitNewWhoTo.bind(this)
  }

  /** Allows to set the states on input */
  handleChange(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  /** Allows to create an object  with all states infos
   * and send the object in parent component with this.props.handleSubmit();
   */
  submitNewWhoTo(e) {
    e.preventDefault();

    const { firstName, lastName, description, skill, github, facebook, twitter } = this.state;

    const newData = {
      firstName,
      lastName,
      description,
      skill,
      github,
      facebook,
      twitter
    }

    this.props.handleSubmit(newData)
  }


  render() {
    /** The toggle modal method from parent */
    const { handleModalState } = this.props;

    /** Methods created inside this component */
    const { submitNewWhoTo, handleChange } = this;
    return (
      <div className="whoToCreate">
        <form className="form" onSubmit={submitNewWhoTo}>
          <div className="field">
            <label className="label">Firstname</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="firstName" type="text" placeholder="Firstname" />
            </div>
          </div>
          <div className="field">
            <label className="label">Lastname</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="lastName" type="text" placeholder="Lastname" />
            </div>
          </div>
          <div className="field">
            <label className="label">description</label>
            <div className="control">
              <textarea required onChange={handleChange} className="textarea" name="description" type="text" placeholder="Description" />
            </div>
          </div>
          <div className="field">
            <label className="label">Spécialité</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="skill" type="text" placeholder="Spécialité" />
            </div>
          </div>
          <div className="field">
            <label className="label">Twitter</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="twitter" type="text" placeholder="Twitter" />
            </div>
          </div>
          <div className="field">
            <label className="label">Github</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="github" type="text" placeholder="Github" />
            </div>
          </div>
          <div className="field">
            <label className="label">Facebook</label>
            <div className="control">
              <input required onChange={handleChange} className="input" name="facebook" type="text" placeholder="Facebook" />
            </div>
          </div>
          <div className="buttonsContainer">
            <button className="button" type="submit">Valider</button>
            <button className="button" onClick={() => handleModalState()} type="button">Annuler</button>
          </div>
        </form>
      </div>
    )
  }
}

export default WhoToFollowCreate
